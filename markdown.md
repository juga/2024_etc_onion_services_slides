
# Contents

- We
- You
- How: (Tor) network human simulation
- Why Onion Services
- Hands on (cooking) Onion Services

---

# We

{gaba, juga} at torproject.org

**you**

---

# You

- Note takers?: <https://pad.riseup.net/p/2024_etc_onion_services.keep>
- Do you use Tor?
- Have you already run Onion Services?
- Do you have a Web page/site you would like others access to?

---

# How: (Tor) network "human" simulation

---

## Location of Tor relays

![image](images/Torflow_map.png)

<https://torflow.uncharted.software/>

---

## 1. Visiting HTTP Web site

![image](images/http.png)

---

## 1. Visiting HTTP Web site

(Instead of Alice and Bob, Aeon and account.riseup.net)

- Aeon, a person in /ETC that opens in a Web browser <http://account.riseup.net>
- traffic pass by German (DE) Internet providers
- traffic pass by US Internet
- traffic arrives to a server hosting account.riseup.net

---

## 2. Visiting HTTPS Web site

![image](images/https.png)

---

## 2. Visiting HTTPS Web site

- Aeon, a person in /ETC  that opens in a Web browser <https://account.riseup.net>
- traffic pass by German (DE) Internet providers
- traffic pass by US Internet
- traffic arrives to a server hosting account.riseup.net

---

## 3. Visiting HTTPS Web site with Tor browser

![image](images/tor.png)

---

## 3. Visiting HTTPS Web site with Tor browser

![image](images/TorBrowser_riseup.png)

---

## 3. Visiting HTTPS Web site with Tor browser

- Aeon, a person in /ETC  that opens in a Web browser <https://account.riseup.net>
- traffic pass by German (DE) Internet providers
- traffic pass by Tor guard relay
- traffic pass by Tor middle relay
- traffic pass by Tor exit relay
- traffic pass by US Internet
- traffic arrives to a server hosting account.riseup.net

---

## 4. Visiting Onion Service

![image](images/TorBrowser_riseup_onion.png)

---

## 4. Visiting Onion Service

- Aeon, a person in /ETC  that opens in a Web browser <https://account.riseup.net>
- traffic pass by German (DE) Internet providers
- traffic pass by Tor guard relay
- traffic pass by Tor middle relay
- traffic pass by Tor Rendez-Vous Point
- traffic pass by Tor relay
- traffic pass by Tor relay
- traffic pass by Tor relay
- traffic pass by US Internet
- traffic arrives to a server hosting account.riseup.net

---

# Why to use onion services

- Location hiding
- End-to-end authentication
- End-to-end encryption
- NAT punching
- No need for public IP

---

![image](images/popular_onion_services.png)

---

# Hands on (cooking onion 🧅 services)

---

## Option 1: temporal, no command line

Windows, macOS, Linux

---

## Server side (the meal to eat)

Ingredients:

- OnionShare
- Any Web site/page file. eg:

```html
<html>
  <head>
    <title>Your first onion service!</title>
  </head>

  <body>
    <h1>Hello onion world!</h1>
  </body>
</html>
```

---

## Server side

Install OnionShare: <https://onionshare.org>

![image](images/onionshare_download.jpg)

---

## Server side

Install OnionShare by command line:

```bash
sudo apt install onionshare
```

---

## Server side

![image](images/onionshare_start.png)

---

## Server side

![image](images/onionshare_website.png)

---

## Server side

![image](images/onionshare_launch.png)

---

## Client (how to eating the meal)

- Install Tor browser: <https://www.torproject.org/download/>

![image](images/torbrowser_mobile.png)

---

## Client

- With Tor browser, go to the address shown by OnionShare

---

# Option 2: permanent, command line

---

## Server side (the meal to eat)

Ingredients:

- A linux Operating System (OS)
- (little-t) tor
- Any Web server:
  - apache2
  - nginx
  - python
  - twisted

---

## Client (how to eating the meal)

- Tor browser

---

# Server side

---

## Option 2a: apache2 cooking

```bash
sudo apt install apache2
```

See whether it's working opening <http://localhost> in any Web browser

---

## Option 2b: nginx cooking

```bash
sudo apt install nginx
```

See whether it's working opening <http://localhost> in any Web browser

---

## Option 2c: Python twistd cooking

```bash
sudo apt install python3-twisted
twistd3 -n web --port "onion:80" --path .
```

---

## Preparing (cooking) little-t tor

```bash
sudo apt install tor
nano /etc/tor/torrc
```

```bash
HiddenServiceDir /var/lib/tor/my-website/
HiddenServicePort 80 127.0.0.1:80
```

---

```bash
sudo systemctl restart tor
cat /var/lib/tor/my-website/hostname
```

---

# Client side

With Tor browser, go to the address you found at /var/lib/tor/my-website/hostname

---

# If all went well

Congrats!, you cooked your first onion service! 🧅🎉

---

# If something is not working

- Onion services install: <https://community.torproject.org/onion-services/setup/install/>

- Tor logs: <https://support.torproject.org/#Logs>

- tor-onions mailing list; <https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-onions>

---

# Many resources and cool examples

- This slides: <https://gitlab.torproject.org/users/juga/2024_etc_onion_services_slides>
- Other more detailed slides; <https://gitlab.torproject.org/tpo/community/training/-/raw/master/2022/onion-services/intro-onion-service-2022.odp?inline=false>, <https://gitlab.torproject.org/tpo/community/training/-/tree/master/2022/onion-services?ref_type=heads>
- Onion services fanzine: <https://community.torproject.org/static/images/outreach/print/onion-guide-fanzine-EN.pdf>

---

- The [Tor design](https://spec.torproject.org/tor-design) paper describing the original design.
- The [Tor v3 Onion Services](https://spec.torproject.org/rend-spec-v3) protocol specification.
- Video presentation: [Onion Services: Understanding Tor Onion Services and Their Use Cases - HOPE XI 2016, DEF CON 25](https://www.youtube.com/watch?v=VmsFxBEN3fc)
- Video presentation: [Roger Dingledine - Next Generation Tor Onion Services](https://www.youtube.com/watch?v=Di7qAVidy1Y)
- Cebollitas: <https://gitlab.torproject.org/tpo/onion-services/cebollitas>
- Onion Desktop: <https://gitlab.torproject.org/tpo/onion-services/onion-desktop>
- Onion Launchpad: <https://gitlab.torproject.org/tpo/onion-services/onion-desktop>

---

- Onion Services Ecosystem: <https://community.torproject.org/onion-services/ecosystem/>, <https://gitlab.torproject.org/tpo/onion-services/ecosystem>

---

# Get help

- Tor Project Forum:
    [[https://forum.torproject.net/c/support/onion-services]](https://forum.torproject.net/c/support/onion-services)
- Tor Browser Manual:
    [[https://tb-manual.torproject.org]](https://tb-manual.torproject.org/)
- Support portal:
    [[https://support.torproject.org/]](https://support.torproject.org/)
- Community team:
    [[https://community.torproject.org/onion-services/]](https://community.torproject.org/onion-services/)

---

# Thank you
