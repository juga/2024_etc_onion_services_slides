---
title: Tor theme for reveal.js
subtitle: A slideshow theme ready for Tor-related presentations
author: The reveal.js Tor team
---

# About

This is a [reveal.js][] theme for [The Tor Project][].

[reveal.js]: https://revealjs.com

[The Tor Project]: https://torproject.org

# Project page

This project is hosted at [https://gitlab.torproject.org/juga/revealjs_tor_theme](https://gitlab.torproject.org/juga/revealjs_tor_theme).

# Example

Check the [example](example.html) for how it looks like.
